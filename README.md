# PocketPCR for the Pipettin-bot

Contents:

[[_TOC_]]

This repository starts as a fork of the PocketPCR project by Gaudi Labs, at its [original repository](https://github.com/GaudiLabs/PocketPCR).

This fork adds/will add:

- [x] Markdown port and Spanish translation of the [docs](./docs).
- [x] A FreeCAD port of the case.
- [ ] 0 cycles for simpler incubations.
- [x] Control through the USB serial interface.
    - [x] Rudimentary JSON parsing using ArduinoJSON.
    - [x] Temperature querying.
    - [x] Full remote control.
- [ ] Small improvements for automation.
- [x] Write code for the lid heater, fixed at 102 degrees. Queda pendiente:
    - [x] Actualizar el valor beta del termistor NTC de la tapa, porque usamos otro (Mouser 815-NTC603103J3950FT).
    - [x] Eliminar el "flicker" mientras corre la PCR.
    - [ ] Hacer que la temperatura de la tapa sea configurable, y emprolijar la UI. Ahora está fija en 102ºC.

![heated_lid.png](./docs/images/heated_lid.png)

> Heated lid, as wired by Fran Quero and Nicolás Méndez.

## Installation

Instructions can be found in the [firmware's README.md](code/PocketPCR_V2_quite_dev_i_disp/README.md) file.

## Assembly

BOM:

- 4-pin cable connector with wires.
- 10K B3950 NTC thermistor (Mouser: 815-NTC603103J3950FT / Abracon ABNTC-0603-103J-3950F-T).
- Solder and soldering iron. A "hot plate" can be useful.

Steps:

1. Add solder to the small pads on the PocketPCR lid. We used a fancy hot plate, but a regular soldering iron can be used instead. Soldering paste can also come in handy.
2. Solder the SMD thermistor to the pads. You can test that it is working by measuring the resistance as it cools.
3. Solder the cable connector to the PocketPCR, and identify the pads on the lid where each wire must connect (see connection diagram below).
4. Solder the signal wires to the corresponding pads on the lid (closest to the thermistor).
5. Solder the Vcc and GND wires to the corresponding pads on the lid (closest to the lid's edge).
6. Place some electrical tape over the thermistor. This will keep the lid's metal spring from accidentally shorting the thermistor circuit.

![hot_plate_pads.png](./docs/images/heated_lid/hot_plate_pads.png)
> Tiny pads for the thermistor.

![IMG_0299.jpg](./docs/images/heated_lid/IMG_0299.jpg)
> Mini soldering hot plate.

![IMG_0302.jpg](./docs/images/heated_lid/IMG_0302.jpg)
> The resistance of the NTC thermistor will increase as the lid cools after soldering.

![IMG_0314.jpg](./docs/images/heated_lid/IMG_0314.jpg)
> Solder a 4-pin connector to the holes on the left side of the PocketPCR.

![wiring.png](./docs/images/heated_lid/wiring.png)
> Detailed view of the connections for the heated lid. The central wires connect to the NTC, while the leftmost and rightmost wires connect to the heater's GND and to 5V USB, respectively.

![IMG_0319.jpg](./docs/images/heated_lid/IMG_0319.jpg)
> Solder wires to the four remaining pads (red/black = Vcc/GND; blue/yellow = signal).

## Usage

The display on the PocketPCR will show the status of the lid.

- To heat the lid, simply connect it's cable to the connector on the PocketPCR.
- To keep the lid cool, disconnect the cable.

Some condensation might still occur. You may want to add some tape around the lid, to form a "skirt" and keep the upper part of the tubes warmer.

## Remote control

A rudimentary JSON-RPC-like API has been added, see:

- Documentation: [API.md](./code/API.md)
- Source: [PocketPCR_V2_quite_dev_i_disp.ino](./code/PocketPCR_V2_quite_dev_i_disp/PocketPCR_V2_quite_dev_i_disp.ino)

# Manufacturing

Instructions to manufacture or order the parts included in the kit.

## Case

Can be 3D-printed.

There is a new FreeCAD file available for free customization: [case.FCStd](./Parts/PocketPCR_Case/case.FCStd)

## Electronics

> WIP!
> Instructions to order the PCB and/or mount SMD components.

## Machined parts

> WIP!
> Instructions to machine the heated block and its base (the star-shaped cooler/dissipator with "fins").

### Heated block

The original drawings for the heated block are in the [Parts](./Parts) directory:

- [PocketPCRHeater_V1.PDF](./Parts/PocketPCRHeater_V1.PDF): possibly for milling on a CNC router.
- [bottom-heater.STEP](./Parts/bottom-heater.STEP): 3D model extracted from the original assembly.

### Block dissipator

The original source form GaudiLabs does not point to a particular file. However, the [PCB_Layouts](./PCB_Layouts) directory has files for [PocketPCR_BottomHeater](./PCB_Layouts/PocketPCR_BottomHeater), and among them is a DXF file for the "heater base":

- [HeaterBase.DXF](./PCB_Layouts/PocketPCR_BottomHeater/svg/HeaterBase.DXF): possibly for laser cutting.
- [bottom-heater.STEP](./Parts/bottom-heater.STEP): 3D model extracted from the original assembly.
