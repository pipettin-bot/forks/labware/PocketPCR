# PocketPCR Kit Instructions

Congratulations on getting the PocketPCR kit. The PocketPCR is the result of many years
of building PCR theremocyclers with do it your self biology communities. Welcome to the
community of people exploring the fantastic world of genetics.

Before using it you need to assemble the PocketPCR kit. You can do it! Just follow the
instructions carefully. Let’s get started.

## Soldering the connector and dial

First thing to do is soldering the fan connector to the board. Place the connector on the
bottom side of the electronic board as shown in the picture below. Watch the correct
orientation. 

![PocketPCR_Instructions_img6.png](./images/PocketPCR_Instructions_img6.png)
![PocketPCR_Instructions_img0.png](./images/PocketPCR_Instructions_img0.png)

![PocketPCR_Instructions_img4.png](./images/PocketPCR_Instructions_img4.png)
![PocketPCR_Instructions_img8.png](./images/PocketPCR_Instructions_img8.png)

Then solder the two pins from the top of the board. Make sure that the two solder joints do
not touch. If you need help with soldering then you find a good guide here:
[https://mightyohm.com/files/soldercomic/FullSolderComic_EN.pdf](https://mightyohm.com/files/soldercomic/FullSolderComic_EN.pdf)

Now with the first soldering exercise done you can go on to mount the digital dial.

![PocketPCR_Instructions_img1.png](./images/PocketPCR_Instructions_img1.png)
![PocketPCR_Instructions_img2.png](./images/PocketPCR_Instructions_img2.png)

Stick the 5 pins of the dial knob through the board from the top and solder from the bottom
side. Finding a good support to hold the board stable helps.

## Fan mount

Next mount the fan using the four long screws, two washers and two spaces. Watch the
orientation of the fan. The cable should be located near the connector as shown on the
picture. Stick the screws through from the top and tighten the washers and screws from
the bottom side.

![PocketPCR_Instructions_img13.png](./images/PocketPCR_Instructions_img13.png)
![PocketPCR_Instructions_img17.png](./images/PocketPCR_Instructions_img17.png)

Then connect the fan cable to the fan connector.

##  Finishing touches

You are almost done. What is left to do is mounting the two missing legs. Use the small
screws and the two long spacers for it.

![PocketPCR_Instructions_img15.png](./images/PocketPCR_Instructions_img15.png)
![PocketPCR_Instructions_img9.png](./images/PocketPCR_Instructions_img9.png)

Finally push the aluminum knob on the shaft of the dial. And screw the rubber feeds on the
end of the legs. The lid cover together with the spring and the long plastic screw serves to
hold down the micro tubes.

![PocketPCR_Instructions_img10.png](./images/PocketPCR_Instructions_img10.png)
![PocketPCR_Instructions_img11.png](./images/PocketPCR_Instructions_img11.png)

## Case 3D Printed (optional)

Optionally you can 3D print a case for your PocketPCR. You can download the 3D model
of the case as STL file. The case is easy to be printed on a regular 3D printer in any color
you like. If you do not own a 3D printer you can upload the file to a 3D printer service and
get it printed and delivered to you.

![PocketPCR_Instructions_img19.png](./images/PocketPCR_Instructions_img19.png)

Download of the case model (STL): [https://www.thingiverse.com/thing:4094823](https://www.thingiverse.com/thing:4094823)

3D-Printing Services:

- imaterialise: [https://i.materialise.com/de](https://i.materialise.com/de)
- Shapeways: [https://www.shapeways.com/](https://www.shapeways.com/)

To assemble the case with your PocketPCR just remove the rubber feeds and then press
the circuit board with a bit of force into the case as shown on the picture.

![PocketPCR_Instructions_img18.png](./images/PocketPCR_Instructions_img18.png)

## Using your PocketPCR

Prepare your PCR reaction with a total volume of 25 – 50 ul. Make sure to cover your liquids in the tubes with 15-30 ul of mineral oil to prevent evaporation. Place a minimum of 3 micro tubes in the heat block. Fix the tubes with the lid.

Connect your PocketPCR to a USB power adapter with a minimum
rating of 2A. Use a good quality USB Type-C cable.

The PCR protocol can be setup through an intuitive graphical interface.
In the main menu choose “Setup” by rotating the knob and then
pressing it.

![PocketPCR_Instructions_img24.png](./images/PocketPCR_Instructions_img24.png)

In the setup menu you can navigate through the PCR protocol
temperatures and timings. Press the knob to edit.

![PocketPCR_Instructions_img21.png](./images/PocketPCR_Instructions_img21.png)

After scrolling through all the parameters a “Set/Cancel” menu is shown,
choose “Set” to store the settings in memory or “Cancel”.

![PocketPCR_Instructions_img23.png](./images/PocketPCR_Instructions_img23.png)

Back in the main menu choose “Run PCR” to start the PCR run.
** Warning. The heatblock will get hot during the PCR run **
During the PCR run the state, cylces, temperatures and times are
displayed . After completion of the protocol the display shows “PCR
Done”. Press to get back to the main menu.

![PocketPCR_Instructions_img22.png](./images/PocketPCR_Instructions_img22.png)

The PCR run can be terminated by rotating the knob. “Press to STOP PCR” will appear in
the top of the display.

By holding down the knob while plugging in the USB plug, the software
version will be displayed. This is also how to start the device in “Left
Handed Mode” with the display flipped.

![PocketPCR_Instructions_img25.png](./images/PocketPCR_Instructions_img25.png)

For more information visit [gaudi.ch/PocketPCR](http://gaudi.ch/PocketPCR)

PocketPCR Instructions by Urs Gaudenz, licensed under a Creative Commons Attribution 4.0 International License.
April 2020

![PocketPCR_Instructions_img27.png](./images/PocketPCR_Instructions_img27.png)
