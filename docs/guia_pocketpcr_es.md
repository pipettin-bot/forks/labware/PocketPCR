# Instrucciones del kit PocketPCR

PocketPCR es un proyecto de hardware científico abierto, y es el resultado de muchos años de construcción de termocicladores para PCR junto a comunidades de biología _DIY_.

# Uso de tu PocketPCR

Prepara tu reacción de PCR en tubos estándar de 0.2 mL, con un volumen total de reacción entre 25 y 50 uL. Asegúrate de cubrir tus líquidos en los tubos con 15-30 uL de aceite mineral, para evitar la evaporación. Coloca un mínimo de 3 microtubos en el bloque de calor, y fija los tubos enroscando la tapa sin usar demasiada fuerza.

Conecta tu PocketPCR a un adaptador de corriente USB con una calificación mínima de 2 A. Utiliza un cable USB tipo C de buena calidad, y que no sea demasiado largo (i.e. menos de 2 metros).

El protocolo de PCR se puede configurar a través de una interfaz gráfica intuitiva. En el menú principal, gira la perilla para seleccionar "Configuración", y luego presionala para entrar al menú de configuración.

![PocketPCR_Instructions_img24.png](./images/PocketPCR_Instructions_img24.png)

En el menú de configuración, puedes girar la perilla para seleccionar las temperaturas y los tiempos del protocolo de PCR. 

Para editar el parámetro seleccionado, primero presiona la perilla y luego gírala para cambiar su valor. Para guardar el nuevo valor, presiona la perilla una vez más.

![PocketPCR_Instructions_img21.png](./images/PocketPCR_Instructions_img21.png)

Después de desplazarte por todos los parámetros, se muestra un menú "Set/Cancel". Elige "Set" para almacenar la configuración en la memoria o "Cancelar" para descartar todos los cambios.

![PocketPCR_Instructions_img23.png](./images/PocketPCR_Instructions_img23.png)

En el menú principal, elige "Run PCR" para iniciar la ejecución de la PCR.

> **Advertencia. El bloque de calor se calentará durante la ejecución de la PCR.**

Durante la ejecución de la PCR, se muestran el estado, los ciclos, las temperaturas y los tiempos. La ejecución de la PCR se puede detener girando la perilla. Al hacerlo aparecerá "Presione para DETENER PCR" en la parte superior de la pantalla.

Después de completar el protocolo, la pantalla mostrará "PCR done". Presiona la perilla para volver al menú principal.

![PocketPCR_Instructions_img22.png](./images/PocketPCR_Instructions_img22.png)

# Parámetros operativos

Temperaturas:

- La temperatura máxima está limitada a 100ºC, y la mínima es la temperatura ambiente.
- La velocidad de enfriado mejorará a una temperatura ambiente menor.
- El equipo tardará más tiempo en llegar a temperaturas muy cercanas a la temperatura ambiente.
- El sensor de temperatura está dentro del bloque. Evita tener corrientes de aire cerca del equipo o usarlo a temperaturas muy bajas, esto puede causar que los tubos no alcancen la temperatura deseada (aunque la pantalla lo indique).

Tiempos:

- El número mínimo de ciclos es 1.
- Para usar el equipo como incubadora, ajuste todas las temperaturas para que sean iguales a la deseada, y ajuste los tiempos y el número de ciclos para que el tiempo de incubación total coincida con el deseado.

# Operación con mano izquierda

El equipo puede ser configurado para usarse más cómodamente con la mano izquierda.

Al mantener presionado la perilla mientras se conecta el enchufe USB, se mostrará la versión del software. También puedes iniciar el dispositivo en el "Modo para zurdos" con la pantalla invertida.

![PocketPCR_Instructions_img25.png](./images/PocketPCR_Instructions_img25.png)


# Armado

Antes de usarlo, debes ensamblar el kit PocketPCR. ¡Puedes hacerlo! Solo sigue las instrucciones cuidadosamente. ¡Comencemos!

## Soldar el conector y el perilla

Lo primero que debes hacer es soldar el conector del ventilador a la placa. Primero coloca el conector en la parte inferior de la placa electrónica como se muestra en la imagen a continuación. Asegúrate de que la orientación sea correcta.

![PocketPCR_Instructions_img6.png](./images/PocketPCR_Instructions_img6.png)
![PocketPCR_Instructions_img0.png](./images/PocketPCR_Instructions_img0.png)
![PocketPCR_Instructions_img8.png](./images/PocketPCR_Instructions_img8.png)

Luego, suelda los dos pines desde la parte superior de la placa. Asegúrate de que las dos uniones de soldadura no se toquen. Si necesitas ayuda con la soldadura, puedes encontrar una buena guía [aquí](https://mightyohm.com/files/soldercomic/FullSolderComic_EN.pdf).

![PocketPCR_Instructions_img4.png](./images/PocketPCR_Instructions_img4.png)

Ahora que has terminado el primer ejercicio de soldadura, puedes continuar montando la perilla o "perilla" digital.

Inserta los 5 pines del perilla a través de la placa desde la parte superior y suelda desde la parte inferior. Ayuda tener un buen soporte (e.g. una cinta de papel) para mantener la placa estable mientras sueldas.

![PocketPCR_Instructions_img1.png](./images/PocketPCR_Instructions_img1.png)
![PocketPCR_Instructions_img2.png](./images/PocketPCR_Instructions_img2.png)


## Montaje del ventilador

A continuación, monta el ventilador utilizando cuatro tornillos largos, dos tuercas y dos espaciadores. Observa la orientación del ventilador. El cable debe estar cerca del conector como se muestra en la imagen. Inserta los tornillos desde la parte superior y aprieta las arandelas y los tornillos desde la parte inferior.

Los tornillos y espaciadores pueden ajustarse con la mano, pero es un poco mejor si se ajustan _suavemente_ usando una pinza. si se ajustan demasiado, las partes de plástico se romperán.

![PocketPCR_Instructions_img13.png](./images/PocketPCR_Instructions_img13.png)
![PocketPCR_Instructions_img17.png](./images/PocketPCR_Instructions_img17.png)

Finalmente, conecta el cable del ventilador al conector del ventilador.

## Toques finales

Ya casi has terminado. Lo que falta es montar las dos patas que faltan. Utiliza dos tornillos pequeños y dos espaciadores largos para armar las patas del otro lado.

![PocketPCR_Instructions_img15.png](./images/PocketPCR_Instructions_img15.png)

Usando las manos, coloca la perilla de aluminio en el eje del perilla, y "atornilla" los pies de goma en el extremo de las patas.

![PocketPCR_Instructions_img9.png](./images/PocketPCR_Instructions_img9.png)

![PocketPCR_Instructions_img10.png](./images/PocketPCR_Instructions_img10.png)

Finalmente, toma el disco perforado, el resorte, y el tornillo de plástico largo, y armar la tapa. Esta sirve para sujetar los microtubos durante la PCR.

![PocketPCR_Instructions_img11.png](./images/PocketPCR_Instructions_img11.png)

## Carcasa impresa en 3D (opcional)

Opcionalmente, puedes imprimir una carcasa en 3D para tu PocketPCR. La carcasa es fácil de imprimir en una impresora 3D común, en cualquier color que desees. 

Para esto puedes descargar el modelo 3D de la carcasa en formato STL, y usar el software de tu impresora para imprimirlo.

Descarga del modelo de la carcasa (STL): [https://www.thingiverse.com/thing:4094823](https://www.thingiverse.com/thing:4094823)

![PocketPCR_Instructions_img19.png](./images/PocketPCR_Instructions_img19.png)

Si no tienes una impresora 3D, y porque es una pieza pequeña, lo más simple es involucrar a un amigo para que la imprima. También puedes cargar el archivo en un servicio comercial de impresión 3D, para que te lo impriman y entreguen.

Servicios de impresión 3D:

- imaterialise: [https://i.materialise.com/de](https://i.materialise.com/de)
- Shapeways: [https://www.shapeways.com/](https://www.shapeways.com/)

Para ensamblar la carcasa con tu PocketPCR, simplemente retira los pies de goma y luego presiona la placa de circuito con un poco de fuerza dentro de la carcasa como se muestra en la imagen.

![PocketPCR_Instructions_img18.png](./images/PocketPCR_Instructions_img18.png)

Finalmente vuelve a colocar los pies de goma desde el lado inferior de la carcasa.

# Más información

Para saber más, visita:

- Versión para el IIBio-UNSAM: https://gitlab.com/pipettin-bot/forks/labware/PocketPCR
- Proyecto original: [gaudi.ch/PocketPCR](http://gaudi.ch/PocketPCR)

# Licencia

Instrucciones para la PocketPCR, escritas por Nicolás A. Méndez y Urs Gaudenz, distribuidas bajo la licencia Creative Commons Attribution 4.0 International License (Junio 2023).

![PocketPCR_Instructions_img27.png](./images/PocketPCR_Instructions_img27.png)