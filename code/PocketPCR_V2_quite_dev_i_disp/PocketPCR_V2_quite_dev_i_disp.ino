/*

PocketPCR PCR Thermocycler by GaudiLabs, forked by naikymen.
A pocket-size USB powered PCR Thermo Cycler.

Written for the SAMD21G18B-A microcontroller (used in the Arduino Zero).

Original designs:
- http://gaudi.ch/PocketPCR/
- https://github.com/GaudiLabs/PocketPCR

Modifications (this version) by Nicolás Méndez:
- https://gitlab.com/pipettin-bot/forks/labware/PocketPCR

Comments sourced in part from ChatGPT:
- https://chat.openai.com/share/422cf24a-5e63-42f2-95e6-24cc47031472

More information:
- Microcontroller datasheet: https://ww1.microchip.com/downloads/aemDocuments/documents/MCU32/ProductDocuments/DataSheets/SAM-D21-DA1-Family-Data-Sheet-DS40001882H.pdf
- Arduino Zero Pinout: https://content.arduino.cc/assets/Pinout-ZERO_latest.pdf
*/

#include <ArduinoJson.h>  // Parse JSON messsages from the serial interface.

#include <math.h> 
#include "Adafruit_GFX.h"
#include <Adafruit_SSD1306.h>
#include "Rotary.h"
#include "FlashStorage.h"

#include <Fonts/FreeSans9pt7b.h>

const char VersionString[] = "V1.02 2020";

// These constants won't change. They're used to give names to the pins used:
const int butPin = A4;      // Analog output pin that the button is attached to.
const float temperature_tollerance=0.5;

// Lid temperature control.
const int lidInPin = A1;    // PB08/A1/D15: Analog input pin that the lid temperature sensor is attached to.
const int lidPin = 9;       // PA07/A6?/D9: Analog output pin that the lid heater is attached to.
bool lid_connected = false;
// NTC parameters.
const int NTC_LID_B = 3435;
const float NTC_LID_TN = 298.15;
const int NTC_LID_RN = 10000;
const float NTC_LID_R0 = 4.7;

// Block temperature control.
const int analogInPin = A2; // Analog input pin that the block temperature sensor is attached to.
const int fanPin = 4;       // Analog output pin that the fan is attached to.
const int heaterPin = 8;    // Analog output pin that the block heater is attached to.
// NTC parameters.
const int NTC_B = 3950;  // 10KOHM 3950K ABNTC-0603-103J-3950F-T (mouser 815-NTC603103J3950FT).
const float NTC_TN = 298.15;
const int NTC_RN = 10000;
const float NTC_R0 = 4.7;


// Rotary encoder is wired with the common to ground and the two
// outputs to pins 6 and 7.
#define ENCODER_1 6
#define ENCODER_2 7
Rotary rotary = Rotary(ENCODER_1, ENCODER_2);

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// If using software SPI (the default case):
#define OLED_MOSI  20
#define OLED_CLK   21
#define OLED_DC    0  // 11
#define OLED_CS    22 // 12
#define OLED_RESET 5

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT,
  OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);
  
#define CASE_Main 1
#define CASE_Settings 2
#define CASE_EditSettings 3
#define CASE_Run 4
#define CASE_Done 5

#define PCR_set 1
#define PCR_transition 2
#define PCR_time 3
#define PCR_end 4


#define PIDp 0.5
#define PIDi 0.0001
#define PIDd 0.15

#define SETTING_Size 6


String SETTING_String[SETTING_Size] = { "Init", "Denature", "Annealing" , "Extension", "Final" };
int SETTING_cycles = 20;

typedef struct {
  int value[SETTING_Size*2];
} EEprom;

FlashStorage(my_flash_store, EEprom);
EEprom settings;

// Block temperature.
int sensorValue = 0;        // value read from the sensor
int outputValue = 0;        // value output to the PWM (heater)
float temperature = 0;
float temperature_mean = 0;
float sensorVoltage = 0;
float sensorResistance = 0;

// Lid temperature.
float temperature_lid = 0;
float temperature_lid_mean = 0;
int sensorValue_lid = 0;     // value read from the sensor
float sensorVoltage_lid = 0;
float sensorResistance_lid = 0;

// Menu variables.
boolean editMode = false;
boolean minuteMode = false;

int counter = 0;        
int counter_save = 0;      

int x = 0;
int caseUX = CASE_Main; 
int casePCR = PCR_set;
int MenuItem = 1;
// PCRstate: Integer in range 0-4.
// Indicates current PCR stage of the PCR program (see SETTING_String for options).
int PCRstate = 0;
int PCRcycle = 1;

// Block temperature control.
float TEMPset;
float TEMPdif;
float TEMPi;
bool PIDIntegration = false;
float TEMPcontrol;
float lidTEMPcontrol;
float TEMPdif_a[10];
float TEMPd;
long TEMPclick=0;

long TIMEclick=0;
int TIMEcontrol=0;

int heatPower = 191;
int fanPower = 0;

// Lid temperature control.
float lid_TEMPset = 102.0;
float lid_TEMPdif;
float lid_TEMPi;
bool lid_PIDIntegration = false;
float lid_TEMPcontrol;
float lid_lidTEMPcontrol;
float lid_TEMPdif_a[10];
float lid_TEMPd;
// long lid_TEMPclick=0; // piggy-back on the Block's time control.


void setup() {

  // Initialize pins.
  pinMode(ENCODER_1, INPUT_PULLUP); // Set ENCODER_1 as an input pin with pull-up resistor.
  pinMode(ENCODER_2, INPUT_PULLUP); // Set ENCODER_2 as an input pin with pull-up resistor.
  pinMode(butPin, INPUT_PULLUP);   // Set butPin as an input pin with pull-up resistor.

  pinMode(fanPin, OUTPUT); // Set fanPin as an output pin.
  pinMode(heaterPin, OUTPUT); // Set heaterPin as an output pin.
  pinMode(lidPin, OUTPUT);    // Set lidPin as an output pin.

  attachInterrupt(ENCODER_1, rotate, CHANGE); // Attach the 'rotate' function to an interrupt on ENCODER_1 when it changes.
  attachInterrupt(ENCODER_2, rotate, CHANGE); // Attach the 'rotate' function to an interrupt on ENCODER_2 when it changes.

  // Initialize serial communications at 115200 bps.
  // Use SerialUSB instead of Serial to send messages, 
  // otherwise no messages will be sent to the serial monitor.
  // https://forum.arduino.cc/t/i-cannot-get-the-serial-print-to-work-in-zero/571174/6
  SerialUSB.begin(115200);
  //while (!SerialUSB) continue;

  // Initialize the display.
  display.begin(SSD1306_SWITCHCAPVCC); // Initialize the OLED display.
  display.clearDisplay();  // Clear the display's buffer.

  // Configure display settings.
  display.setTextSize(1);  // Set the text size.
  display.setTextColor(WHITE);  // Set text color to white.
 
  // Print software version on the display.
  display.setCursor(15, 20);
  display.println("Software Version");
  display.setCursor(30, 30);
  display.println(VersionString);

  // Print left-handed mode on the display.
  display.setCursor(15, 45);
  display.println("Left Handed Mode");
  display.dim(false); // Turn off display dimming.
  display.display(); // Display the content.
  // display.setFont(&FreeMono9pt7b);

  // Check for left-handed mode using the button.
  if (!digitalRead(butPin)) {
    while (!digitalRead(butPin)); // Wait for the button press to stabilize.
    display.setRotation(2); // Set display rotation for left-handed mode.
  } else {
    display.setRotation(0); // Set display rotation for right-handed mode (0 for right-handed, 2 for left-handed).
  }

  // Set sensor resolution to 12 bits.
  analogReadResolution(12);


  // Read settings from persistent storage.
  // If settings haven't been initialized (ever), set default values and save to persistent storage.
  // This means that previously configured parameters are preserved. These are only factory defaults.
  settings=my_flash_store.read();
  if (settings.value[11]==0){
    // Initial denaturation temperature in degrees Celsius.
    settings.value[0] = 94;
    // Initial denaturation time in seconds.
    settings.value[1] = 60;

    // Denaturation temperature in degrees Celsius.
    settings.value[2] = 94;
    // Denaturation time in seconds.
    settings.value[3] = 120;

    // Annealing temperature in degrees Celsius.
    settings.value[4] = 55;
    // Annealing time in seconds.
    settings.value[5] = 80;

    // Extension temperature in degrees Celsius.
    settings.value[6] = 72;
    // Extension time in seconds.
    settings.value[7] = 40; 

    // Temperature for other PCR steps in degrees Celsius.
    settings.value[8] = 25;      
    // Time for other PCR steps in seconds.
    settings.value[9] = 180;  
    
    // Number of PCR cycles to run.
    settings.value[10] = 25;
    
    // Mark settings as initialized.  
    settings.value[11] = 1;
    
    // Save the initialized settings to persistent storage.
    my_flash_store.write(settings);
  };
}

void loop() {

  if(SerialUSB){
    // Send alive signal.
    send_alive();
    
    // Echo incoming serial messages.
    // echo_serial();

    // Parse JSON messages
    parse_json_from_serial();
  }
  
  // Disable heaters before read.
  setHeater(0, 0, heaterPin, fanPin);
  setHeater(0, 0, lidPin, -1);

  // analogReference(AR_EXTERNAL);

  // Block temperature control.
  // Read the analog in value:
  sensorValue = analogRead(analogInPin);

  // Lid temperature control  
  // Read the analog in value:
  sensorValue_lid = analogRead(lidInPin);

  // Compute temprature from signal.
  sensorVoltage = 3.3*sensorValue/4096;
  sensorResistance = ((sensorVoltage*NTC_R0)/(3.3-sensorVoltage));
  temperature = 1/(log(sensorResistance*1000/NTC_RN)/NTC_B+1/NTC_TN)-273.15 ;
  temperature_mean = (temperature_mean*3+temperature)/4;
  setHeater(temperature_mean, TEMPcontrol, heaterPin, fanPin);

  // Lid temperature control
  sensorVoltage_lid = 3.3*sensorValue_lid/4096;
  sensorResistance_lid = ((sensorVoltage_lid*NTC_LID_R0)/(3.3-sensorVoltage_lid));
  temperature_lid = 1/(log(sensorResistance_lid*1000/NTC_LID_RN)/NTC_LID_B+1/NTC_LID_TN)-273.15 ;
  temperature_lid_mean = (temperature_lid_mean*3+temperature_lid)/4;
  setHeater(temperature_lid_mean, lidTEMPcontrol, lidPin, -1);

  // print the results to the Serial Monitor:
  // SerialUSB.print("sensor = ");
  // SerialUSB.println(sensorValue_lid);
  // Serial.print("\t output = ");
  // Serial.println(temperature);

  switch (caseUX) {

    // Draw main menu.
    case CASE_Main:
      // Limit counter between 1 or 0 for the main display.
      if (counter>1) counter=0;
      if (counter<0) counter=1;
      MenuItem = counter; // Set the MenuItem variable to the value of counter.
      draw_main_display(); // Call the function to draw the main menu.
      
      // If the button is not pressed.
      if (!digitalRead(butPin)){
        while (!digitalRead(butPin)); // Wait until the button is pressed.
        // Note that the encoder uses interrupts and thus works even though the code hangs here.
        if (MenuItem==0) {
          caseUX = CASE_Run; // If the menu item is 0, switch to the "Run" case.
          casePCR = PCR_set; // Set the PCR state to "PCR_set."
          PCRstate = 0; // Reset the PCR state to 0.
          counter = 0; // Reset the counter to 0.
        }
        if (MenuItem==1) {
          caseUX = CASE_Settings; // If the menu item is 1, switch to the "Settings" case.
          counter = 0; // Reset the counter to 0.
        }
      }
      
      break;
    
    // Draw settings menu.
    case CASE_Settings:

        if (counter > 12) counter = 0; // Ensure that counter is within the range of 0 to 12.
        if (counter<0) counter=12;
        MenuItem = counter; // Set the MenuItem variable to the value of counter.

        // Check whether the MenuItem is an odd number and if the associated setting value is greater than 90.
        if ((MenuItem % 2 == 1) && (settings.value[MenuItem] > 90)) {
          minuteMode = true; // Set minuteMode to true if the condition is met.
        } else {
          minuteMode = false; // Otherwise, set minuteMode to false.
        }

        // If the button is not pressed.
        if (!digitalRead(butPin)) {
          // Wait until the button is pressed.
          while (!digitalRead(butPin));
          caseUX = CASE_EditSettings; // If a button press is detected, switch to the "EditSettings" case.
          if (minuteMode) {
            counter = settings.value[MenuItem] / 60; // Set the counter based on the setting value in minutes.
          } else {
            counter = settings.value[MenuItem]; // Set the counter based on the setting value.
          }
        }
        draw_setup_display(); // Call the function to draw the settings menu.

        break;

    // Draw edit settings menu.
    case CASE_EditSettings:
        // PCR configuration.
        
        // Even menu items are temperatures and cycle number.
        if (MenuItem%2==0){
          if (MenuItem==10) {
            // Limit cycles between 1 and 99.
            if (counter<1) counter=1;
            if (counter>99) counter=99;
          } else {
            // Limit temperatures between 25 and 99.
            if (counter<25) counter=25;
            if (counter>99) counter=99;
          }
        } // Temp Menu
        
        // Odd menu items are temperatures and cycle number.
        else {
          if (minuteMode){
            if (counter<2) {minuteMode=false; counter=90;}
          }  //Minute Mode
          else {
            if (counter>90) {minuteMode=true; counter=2;}
            if (counter<0) {counter=0;}
          } //not Minute Mode
        } // Time Menu
        
        // Check if MenuItem is 11 (done editing), and then return to the Main menu.
        if (MenuItem == 11) {
            caseUX = CASE_Main;
            my_flash_store.write(settings); // Save the settings to memory.
        }
        // Check if MenuItem is 12 (cancel editing), and then return to the Main menu.
        if (MenuItem == 12) {
            caseUX = CASE_Main;
            settings = my_flash_store.read(); // Revert to the previous settings from memory.
        } 
          
        // If MenuItem is less than 11 (temperature or time setting), update the corresponding setting in the settings array.
        if (MenuItem < 11) {
            if (minuteMode)
                settings.value[MenuItem] = counter * 60; // Convert minutes to seconds and store in settings.
            else
                settings.value[MenuItem] = counter; // Store the time or temperature value in settings.
        }
        
        // Check if the button is pressed.
        if (!digitalRead(butPin)) {
            while (!digitalRead(butPin)); // Wait for the button to be released.
            caseUX = CASE_Settings; // Switch to the Settings menu.
            counter = MenuItem; // Set the counter to the current MenuItem.
        }
        
        draw_setup_display(); // Redraw the setup display with updated settings.
        
        break;

    // Draw PCR run menu.
    case CASE_Run:

      // Block temperature control.
      TEMPdif=TEMPset-temperature_mean;
      TEMPi=TEMPi+(TEMPset-temperature_mean);

      // Lid temperature control.
      lid_TEMPdif = lid_TEMPset-temperature_lid_mean;
      lid_TEMPi = lid_TEMPi+(lid_TEMPset-temperature_lid_mean);
      
      // Check if more than 200ms have passed since the last click.
      if (millis()-TEMPclick>200) {
        TEMPclick=millis();

        // Block temperature control.
        TEMPd=TEMPdif_a[4]-TEMPdif;
        // Shift the past temperature differences in an array to calculate the derivative term.
        TEMPdif_a[4]=TEMPdif_a[3];
        TEMPdif_a[3]=TEMPdif_a[2];
        TEMPdif_a[2]=TEMPdif_a[1];
        TEMPdif_a[1]=TEMPdif_a[0];
        TEMPdif_a[0]=TEMPdif;
        
        // Lid temperature control.
        lid_TEMPd=lid_TEMPdif_a[4]-lid_TEMPdif;
        // Shift the past lid temperature differences in an array for the derivative term.
        lid_TEMPdif_a[4]=lid_TEMPdif_a[3];
        lid_TEMPdif_a[3]=lid_TEMPdif_a[2];
        lid_TEMPdif_a[2]=lid_TEMPdif_a[1];
        lid_TEMPdif_a[1]=lid_TEMPdif_a[0];
        lid_TEMPdif_a[0]=lid_TEMPdif;

        //  Serial.println (TEMPd);
      }
      
      switch (casePCR) {
        
        // Update temperature setpoint and time until the next transition.
        case PCR_set:
          // Get temperature and time settings for the current PCR "state".
          // The state is an int in range 0-4 corresponding to the current PCR stage: 
          // 0: "Init", 1: "Denature", 2: "Annealing" , 3: "Extension", 4: "Final"
          TEMPset=settings.value[PCRstate*2];
          TIMEcontrol=settings.value[1+PCRstate*2];
          
          PIDIntegration=false;
          lid_PIDIntegration=false;
          
          // Transition to the next PCR state.
          casePCR=PCR_transition;
          break;

        // Transition sequence, from one setpoint to another.
        // Run the PID alcorithm until the new setpoint is reached,
        // and then move on to the "waiting" phase.
        case PCR_transition:
          // Run the PID control for block temperature.
          runPID();
          // Run the PID control for lid temperature.
          runLidPID();
          // Draw the PCR run display.
          draw_run_display();
          
          // Check if the block temperature is within tolerance.
          if (abs(TEMPset-temperature_mean)<temperature_tollerance) {
            PIDIntegration=true;
            TEMPi=0;
            TIMEclick=millis();
            // Move to the time control state.
            casePCR=PCR_time;
          }
          // Check if the lid temperature is within tolerance.
          if (abs(lid_TEMPset-temperature_lid_mean)<temperature_tollerance) {
            lid_PIDIntegration=true;
            lid_TEMPi=0;
            // Commented out lines: do not alter regular cycling behaviour because of the lid.
            // TIMEclick=millis();
            // casePCR=PCR_time;
          }
          break;

        // Once a new target temperature is reached, run the PID until
        // the current temperature step's time is up.
        case PCR_time:
          // Run the PID control for block temperature.
          runPID();
          // Run the PID control for lid temperature.
          runLidPID();
          // Calculate remaining time for the current PCR state.
          TIMEcontrol=settings.value[1+PCRstate*2]-(millis()-TIMEclick)/1000;
          // Draw the PCR run display.
          draw_run_display();

          // Check if time is up for the current state. 
          if (TIMEcontrol<=0) {
            // Terminate the PCR if the state corresponds to "Final".
            if (PCRstate==4) caseUX=CASE_Done;
            // Return the state to "Denature".
            if (PCRstate==3){
              PCRstate=1; // Denature.
              PCRcycle++; // Count a cycle.
              // Proceed to the "final" state then all PCR cycles have elapsed.
              if (PCRcycle>settings.value[10]) PCRstate=4;
            } else {
              // Increase the PCR state if it is not 3 (Ext.) nor 4 (Final).
              // That is, when initial, denaturing or annealing.
              PCRstate++;
            };
            // If time has elapsed, flag a transition to the next temperature state.
            casePCR=PCR_set;
          }
          break;
        
        default:
          // Handle any other cases.
          break;
        
        }//PCR switch

      break;

    // Draw PCR done menu.
    case CASE_Done:
      // Set block temperature control to 0.
      TEMPcontrol=0;
      // Apply the temperature control settings to the block heater.
      setHeater(temperature_mean, TEMPcontrol, heaterPin, fanPin);

      // Set lid temperature control to 0.
      lidTEMPcontrol=0;
      // Apply the temperature control settings to the lid heater.
      setHeater(temperature_lid_mean, lidTEMPcontrol, lidPin, -1);

      // Clear the display.
      display.clearDisplay(); 
      display.setFont(&FreeSans9pt7b);
      
      // Draw a filled rectangle with "PCR Done" text.
      display.fillRect(10,10,100,40,1);
      display.setCursor(15,30);
      display.setTextColor(0);
      display.println("PCR Done");
      display.display(); 
      
      // Reset the font.
      display.setFont();

      // Check for a button press to return to the main menu.
      if (!digitalRead(butPin)) {
        while (!digitalRead(butPin));
        // Transition back to the main menu.
        caseUX=CASE_Main;
        counter=MenuItem;
      }

      break;
          
    default:
      // Handle any other cases.
      break;
        
  } //switch
}

// Send alive message if a second has elapsed.
uint32_t timestamp = 0;
void send_alive(){
  uint32_t now = millis();
  if ((now - timestamp) >= 3000) {  // Handles millis rollover/overflow correctly.
    // Create JSON object.
    StaticJsonDocument<256> status_doc;
    // Specity method.
    status_doc["method"] = "status";
    
    // Create data and add to doc.
    JsonObject data = status_doc.createNestedObject("data");
    // Add device state info.
    // Integer representation of state, might take the value of the CASE_Main variable for example.
    data["state"] = caseUX;
    // Add PCR program status information.
    data["pcr_state"] = casePCR;   // Set, transtion, or time.
    data["pcr_stage"] = PCRstate;  // 0: "Init", 1: "Denature", 2: "Annealing" , 3: "Extension", 4: "Final"
    data["pcr_cycle"] = PCRcycle;  // Current cycle number.
    // Add power output variables:
    data["heater_power"] = heatPower;  // Current heater block power.
    data["fan_power"] = fanPower;  // Current fan power.

    // Add timestamp.
    data["timestamp"] = now;
    
    // Create nested array for settings.
    JsonArray data_settings = data.createNestedArray("settings");

    // Create a for loop to iterate through each setting and add it to the "data" object.
    for (int i = 0; i < SETTING_Size*2; i++) {
      // Add the setting value to JSON array.
      data_settings.add(settings.value[i]);
    }

    // Generate the minified JSON and send it to the Serial port.
    serializeJson(status_doc, SerialUSB);
    // Start a new line.
    //SerialUSB.println();

    // Update the timestamp.
    timestamp = now;
  }
}

// Echo messages received through serial.
void echo_serial(){
  // Send data only when you receive data: https://gist.github.com/Protoneer/96db95bfb87c3befe46e
  int incomingByte = 0;    // for incoming serial data
  if (SerialUSB.available() > 0) {
    while(SerialUSB.available() > 0){
      // read the incoming byte:
      incomingByte = SerialUSB.read();
      // say what you got:
      SerialUSB.print((char)incomingByte);
    }
    // SerialUSB.println();
  }
}

// Deserialize a JSON document with ArduinoJson.
// https://arduinojson.org/v6/example/parser/
void parse_json_from_serial() {
  // Check if the computer is transmitting.
  while(SerialUSB.available()) {
    
    // Skip newline characters.
    // skip_nl_cr();
    int next_byte = SerialUSB.peek();
    if(next_byte == 10 || next_byte == 13){
      // Remove the NL/CR byte from the serial buffer.
      SerialUSB.read();
    
    // Parse JSON and call methods.
    } else {
      // Allocate the JSON document for the incoming message.
      // This one must be bigger than the sender's because it must store the strings.
      //
      // Inside the brackets, 200 is the RAM allocated to this document.
      // Don't forget to change this value to match your requirement.
      // Use https://arduinojson.org/v6/assistant to compute the capacity.
      StaticJsonDocument<300> doc;

      // StaticJsonObject allocates memory on the stack, it can be
      // replaced by DynamicJsonDocument which allocates in the heap.
      //
      // DynamicJsonDocument  doc(200);

      // Read the JSON document from the "link" serial port.
      // Note: JSON reading terminates when a valid JSON is received, not considering line breaks.
      // Adding them to the message will result in incomplete/empty errors from deserializeJson.
      DeserializationError err = deserializeJson(doc, SerialUSB);

      // Allocate the JSON document for the response.
      StaticJsonDocument<200> response_doc;

      // Check if parsing went well.
      if (err == DeserializationError::Ok){
        
        // If so, get the method's name.
        String method = doc["method"] | "none";
        
        // // Check if the parsed JSON has a "method" field
        // if (doc.containsKey("method")) {
        //   const char* method = doc["method"];
        //   SerialUSB.print("Method: ");
        //   SerialUSB.println(method);
        // }
        
        // Insert the ID or a default one if it is missing.
        bool has_id = doc.containsKey("id"); // true
        if(!has_id){
          response_doc["id"] = (int)-1;
        } else {
          response_doc["id"] = doc["id"];
        }

        // Then route the contents to the appropriate local function.
        if (method == "none"){
          response_doc["message"] = "No method selected";
        
        // Example method.
        } else if(method == "example"){
          // The following document is expected:
          // {"method": "example", "timestamp": 1234, "value": 687, "id": 10}
          // See: https://arduinojson.org/v6/how-to/do-serial-communication-between-two-boards/

          // Add incoming values to the output document.
          response_doc["timestamp"] = doc["timestamp"].as<long>();
          response_doc["value"] = doc["value"].as<int>();

        // Get data method.
        } else if (method == "get"){
          // The following document is expected:
          // {"method":"get", "id": 42}
          JsonObject data = doc.createNestedObject("data");

          // Add values to the document.
          data["lid"] = temperature_mean;
          data["blk"] = temperature_lid_mean;
          data["blkset"] = TEMPset;
          data["lidset"] = lid_TEMPset;

          // Add device state info.
          data["state"] = caseUX;  // Integer representation of state,
          // Add PCR program status information.
          data["pcr_state"] = casePCR;   // Set, transtion, or time.
          data["pcr_stage"] = PCRstate;  // 0: "Init", 1: "Denature", 2: "Annealing" , 3: "Extension", 4: "Final"
          data["pcr_cycle"] = PCRcycle;  // Current cycle number.

        // State select.
        } else if (method == "set_state") {
          // The following document is expected:
          // {"method":"set_state", "data": {"state": "some valid state"}}
          
          // Get new state.
          String new_state = doc["data"]["state"].as<String>(); // "run"
          
          // State select.
          if (new_state == "run") {
            // The following document is expected:
            // {"method":"set_state", "data": {"state": "run"}}
            caseUX=CASE_Run;
          // Run method.
          } else if (new_state == "stop") {
            // The following document is expected:
            // {"method":"set_state", "data": {"state": "stop"}}
            caseUX=CASE_Done;
          // Run method.
          } else if (new_state == "settings") {
            // The following document is expected:
            // {"method":"set_state", "data": {"state": "settings"}}
            caseUX=CASE_Settings;
          // Run method.
          } else if (new_state == "main") {
            // The following document is expected:
            // {"method":"set_state", "data": {"state": "main"}}
            caseUX=CASE_Main;
          } else {
            // Error report.
            response_doc["error"] = "Invalid state specification.";
          }

        // Set config method.
        } else if (method == "configure") {
          // The following document is expected:
          // {"method": "configure", "data": {"settings": [1,2,3,4,5,6,7,8,9,10,11,12]}}
          // {"method": "configure", "data": {"settings": [94,60,94,120,55,80,72,40,25,180,25,1]}}
          
          // Get the new settings array.
          JsonArray new_settings = doc["data"]["settings"];
          // Create a for loop to iterate through each setting and add it to the "data" object.
          for (int i = 0; i < SETTING_Size*2; i++) {
            // Save the setting value from the JSON array.
            settings.value[i] = new_settings[i];
          }
        } else {
          // Do nothing if no method was matched.
          response_doc["warning"] = "Method not matched.";
        }

        // Generate the minified JSON and send it to the Serial port.
        serializeJson(response_doc, SerialUSB);
        // Start a new line.
        //SerialUSB.println();

      } else {
        // If parsing failed, respond with a JSON document with information.
        String message = "Deserialization error: ";
        message = message + err.c_str();
        response_doc["error"] = message;
        // Send response
        serializeJson(response_doc, SerialUSB);
        // Start a new line.
        //SerialUSB.println();

        // Flush all bytes in the "link" serial port buffer.
        while (SerialUSB.available() > 0)
          SerialUSB.read();
      }
    }
    // Parse the next message.
  }
}

// Peek on the next byte from the SerialUSB interface,
// and skip it if it is a newline or carriage return.
// void skip_nl_cr(){
//   while(true){
//     int next_byte = SerialUSB.peek();
//     if(next_byte == 10 || next_byte == 13){
//       // Newline '\n' has code 10.
//       // For comparisons, a character is enclosed in single quotes ('\n') not double quotes.
//       // https://www.cs.cmu.edu/~pattis/15-1XX/common/handouts/ascii.html
//       // https://forum.arduino.cc/t/recognizing-new-line-character/619160/2
//       // https://forum.arduino.cc/t/how-to-check-cr-in-the-char-from-serial-read-function/180110/4
//       
//       // Remove the NL/CR byte from the serial buffer.
//       SerialUSB.read();
// 
//       // Prepare a response document.
//       // StaticJsonDocument<96> response_doc;
//       // String message = "Skipping newline character: ";
//       // response_doc["method"] = "message";
//       // response_doc["data"] = message + next_byte;
//       // serializeJson(response_doc, SerialUSB);
//       // SerialUSB.println();
//     } else {
//       break;
//     }
//   }
// }

void draw_lid_status(){
  // Info: https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives
  
  display.setCursor(128 - 10,0);

  if(lid_connected){
    // Print a "hat" and a dark background.
    display.setTextColor(WHITE, BLACK);
    display.write(0x1e);
    display.setCursor(128 - 10*6, 0);
    display.print("Lid ON");
  } else {
    // Print "void".
    // display.setTextColor(BLACK, BLACK);
    // display.write(0xda);
    // Print "empty".
    display.setTextColor(WHITE, BLACK);
    display.write(0xec);
    display.setCursor(128 - 10*6, 0);
    display.print("Lid OFF");
  }

  display.display();
}

bool update_lid_status(int sensor_value){
  if(sensor_value >= 4096 - 30) 
    lid_connected = false;
  else
    lid_connected = true;

  // draw_lid_status();

  return lid_connected;
}


void draw_value_box(int x, int y, int val, bool highlight,bool edit){
  // x,y: position.
  // val: value of the setting.
  // highlight: result of comparing MenuItem to an int to check if it should be highlighted (selected).
  // edit: result of comparing "caseUX == CASE_EditSettings" indicating if the value is in edit mode.

  int shift;  // Declare an integer variable 'shift'

  // Determine whether to shift the value box based on 'val' and 'x' position
  if ((val < 46) && (x > 22) && (x < 100)) {
    shift = -13;  // Set 'shift' to -13 if conditions are met
  } else {
    shift = 0;  // Otherwise, set 'shift' to 0
  }
  
  // Draw the outline of the value box
  display.drawRect(x - 3, y + 2 - val / 2, 17, val / 2 - 1, 1);
  
  // If 'highlight' is true and 'edit' is false, perform the following:
  if (highlight && !edit) {
    // Fill the value box with a highlight (assuming 11 pixels in height)
    display.fillRect(x - 3, y + 2 - val / 2 + shift, 17, 11, 1);
    display.setTextColor(0);  // Set text color to 0 (typically, black)
  }
  
  // If 'highlight' is true and 'edit' is true, perform the following:
  if (highlight && edit) {
    // Draw a border around the value box (assuming 11 pixels in height)
    display.drawRect(x - 2, y + 2 - val / 2 + shift, 15, 11, 1);
  }
  
  // Set the text cursor position for displaying 'val' inside the value box
  display.setCursor(x, y - val / 2 + 4 + shift);

  // Display the value 'val' inside the box
  display.println(val);

  display.setTextColor(1);  // Reset text color to 1 (typically, white)
};

void draw_value(int x, int y, int val, bool highlight, bool edit) {
  // If 'highlight' is true and 'edit' is false, perform the following:
  if (highlight && !edit) {
    // Fill a rectangular area as a background for the value box (assumed 17x12 pixels) with color 1
    display.fillRect(x - 3, y - 1, 17, 12, 1);
    display.setTextColor(0);  // Set the text color to 0 (typically, black)
    display.setCursor(x, y - 1);  // Set the text cursor position at (x, y - 1)
  } else {
    // If 'highlight' is not true or 'edit' is true, set the text cursor at (x, y) without a background
    display.setCursor(x, y);
  }

  // If 'highlight' is true and 'edit' is true, draw vertical lines to indicate an edit mode border
  if (highlight && edit) {
    display.drawLine(x - 3, y, x - 3, y + 6, 1);  // Left line
    display.drawLine(x + 13, y, x + 13, y + 6, 1);  // Right line
  }
  
  // Depending on the value 'val', the following text will be displayed:
  if (val > 90) {
    // If 'val' is greater than 90, print 'val/60' (assumed to be in minutes) and "m"
    display.print(val / 60); 
    display.println("m");
  } else {
    // If 'val' is 90 or less, simply display 'val'
    display.println(val); 
  }
  
  display.setTextColor(1);  // Reset the text color to 1 (typically, white) for subsequent drawing operations
}

void draw_setup_display() {
  // Clear the display to prepare for drawing
  display.clearDisplay(); 
  
  if (MenuItem<11) {
    display.setCursor(5,0);

    if (MenuItem<10){
      display.print(SETTING_String[MenuItem/2]);
      if (MenuItem%2==0) {
        display.print(" Temp. ["); // For even MenuItem, display temperature setting.
        display.print("\xA7"); // Display the degree symbol (°).
        display.print("C]"); // Display temperature unit (°C).
      } else {
        display.print(" Time "); // For odd MenuItem, display time setting.
        if (minuteMode)
          display.print("[min]"); // Display time unit as minutes if minuteMode is true, otherwise in seconds.
        else
          display.print("[s]"); // Display time unit as seconds.
      }
    } else {
      display.print("No of cycles"); // Display the option for setting the number of cycles.
    }

  } else {
    // Set or Cancel options.
    if (MenuItem == 12) {
      // Cancel option selected.
      display.setCursor(34, 0);
      display.println("Set"); // Display "Set" as the option.
      display.fillRect(70, 0, 42, 9, 1); // Draw a filled rectangle for the "Cancel" option.
      display.setTextColor(0); // Set text color to black for "Cancel."
      display.setCursor(75, 1);
      display.println("Cancel"); // Display "Cancel."
      display.setTextColor(1); // Set text color back to white.
    } else {
      // Set option selected.
      display.fillRect(30, 0, 25, 9, 1); // Draw a filled rectangle for the "Set" option.
      display.setTextColor(0); // Set text color to black for "Set."
      display.setCursor(34, 1);
      display.println("Set"); // Display "Set."
      display.setTextColor(1); // Set text color back to white.
      display.setCursor(75, 0);
      display.println("Cancel"); // Display "Cancel."
    }
  }

  // Draw value boxes for denaturation, annealing, extension, and number of cycles settings.
  draw_value_box (21,55,settings.value[0],MenuItem==0,caseUX==CASE_EditSettings);  // Initial denaturation temperature.
  draw_value_box (43,55,settings.value[2],MenuItem==2,caseUX==CASE_EditSettings);  // Cycle denaturation temperature.
  draw_value_box (63,55,settings.value[4],MenuItem==4,caseUX==CASE_EditSettings);  // Cycle annealing temperature.
  draw_value_box (83,55,settings.value[6],MenuItem==6,caseUX==CASE_EditSettings);  // Cycle extension temperature.
  draw_value_box (105,55,settings.value[8],MenuItem==8,caseUX==CASE_EditSettings); // Final extension temperature.

  // Draw a frame for the entire settings area.
  display.drawRect(38,45,61,11,1);
  display.fillRect(38+1,45+1,61-2,11-2,0); // Draw a filled rectangle within the frame.


  if (MenuItem == 10 && !(caseUX == CASE_EditSettings)) {
    display.fillRect(56, 45, 25, 11, 1); // Draw a filled rectangle for the "x" number.
    display.setTextColor(0); // Set text color to black.
  }
  if (MenuItem == 10 && (caseUX == CASE_EditSettings)) {
    display.drawRect(56, 45, 25, 11, 1); // Draw a frame for the "x" number when in edit mode.
  }

  display.setCursor(60, 47);
  display.print(settings.value[10]); // Display the number of cycles.
  display.print("x"); // Display "x" to represent cycles.
  display.setTextColor(1); // Set text color back to white.

  // Draw value boxes for the time settings.
  draw_value(21, 57, settings.value[1], MenuItem == 1, caseUX == CASE_EditSettings);  // Initial denaturation time.
  draw_value(43, 57, settings.value[3], MenuItem == 3, caseUX == CASE_EditSettings);  // Cycle denaturation time.
  draw_value(63, 57, settings.value[5], MenuItem == 5, caseUX == CASE_EditSettings);  // Cycle annealing time.
  draw_value(83, 57, settings.value[7], MenuItem == 7, caseUX == CASE_EditSettings);  // Cycle extension time.
  draw_value(105, 57, settings.value[9], MenuItem == 9, caseUX == CASE_EditSettings); // Final extension time.

  display.display();
}

void draw_main_display(){
  // Clear the display to prepare for new content.
  display.clearDisplay(); 

  // Set the font for text on the display.
  display.setFont(&FreeSans9pt7b);

  // Set or Cancel
  // Check if the MenuItem (menu selection) is 0.
  if (MenuItem == 0) {
    // If MenuItem is 0, highlight "Run PCR" option.
    // Draw a filled rectangle behind "Run PCR" text to indicate selection.
    display.fillRect(21, 10, 86, 25, 1);
    // Set the text color to inverted (0) and position the cursor.
    display.setTextColor(0);
    display.setCursor(26, 28);
    // Display "Run PCR" in inverted color.
    display.println("Run PCR");

    // Highlight "Setup" as an unselected option.
    display.setTextColor(1);
    display.setCursor(40, 55);
    display.println("Setup");
  } else {
    // If MenuItem is not 0, highlight "Setup" instead.
    display.setTextColor(1);
    display.setCursor(26, 28);
    display.println("Run PCR");

    // Draw a filled rectangle behind "Setup" text to indicate selection.
    display.fillRect(21, 38, 86, 25, 1);
    // Set the text color to inverted (0) and position the cursor.
    display.setTextColor(0);
    display.setCursor(40, 55);
    // Display "Setup" in inverted color.
    display.println("Setup");
  }
  
  // Reset the font to the default.
  display.setFont();

  // Set text color back to the default (1).
  display.setTextColor(1);

  // Update the status of the lid and display it on the main menu.
  update_lid_status(sensorValue_lid);
  draw_lid_status();

  // Display the updated content on the screen.
  display.display();
}


void draw_run_display(){
  // Clear the display to prepare for new content.
  display.clearDisplay(); 

  // Draw a filled black rectangle at the top of the screen.
  display.fillRect(0, 0, 128, 11, 1);
  // Set the cursor position and text color to inverted (0).
  display.setCursor(16, 2);
  display.setTextColor(0);
  
  // Depending on the value of 'counter' (even or odd), display different messages.
  if (counter%2==0){
    // Display "PCR Running" when the counter is even.
    display.println("  PCR Running");
  } else {
    // Display "Press to STOP PCR" when the counter is odd.
    display.println("Press to STOP PCR");
    // If the button is pressed, set 'caseUX' to 'CASE_Done' and reset 'counter' to 'MenuItem'.
    if (!digitalRead(butPin)) {
      // Check if the button is pressed.
      while (!digitalRead(butPin));
      // Update caseUX to CASE_Done and reset the counter to MenuItem.
      caseUX = CASE_Done;
      counter = MenuItem;
    }
  }
  
  // Reset the text color to the default (1).
  display.setTextColor(1);

  // Set the cursor position for the following information.
  display.setCursor(0,14);
 
 // Display the current PCR state (e.g., Denaturation, Annealing).
  display.print("State: ");
  display.print(SETTING_String[PCRstate]);
  display.setCursor(0, 23);
  display.print("Cycle: ");
 
  if ( (PCRstate > 0) && (PCRstate < 4)){
    // Display the current cycle information for specific states.
    display.print(PCRcycle);
    display.print(" of ");
    display.print(settings.value[10]);
  }

  // Draw a horizontal line.
  display.drawLine(0,34,128,34,1);

  // Display settings related to temperature and time.
  display.setCursor(0,55-18);
  display.print("Set Temp: ");
  display.print(settings.value[PCRstate*2]);
  display.print(" \xA7");  // Degree symbol.
  display.print("C");

  display.setCursor(0,55-9);
  display.print("Block Temp: ");
  display.print(temperature_mean, 1);  // Display temperature with one decimal place.
  display.print(" \xA7");  // Degree symbol.
  display.print("C");

  display.setCursor(0,55);
  display.print("Time: ");
  display.print(TIMEcontrol);
  //display.print(" ");
  //display.print(TEMPcontrol);
  // display.print(" ");
  // display.print(heatPower);
  // display.print();
  display.print(" s");

  // Draw a temperature indicator bar.
  display.setCursor(1,25);
  display.drawLine(x,10*(temperature_mean-25),x,10*(temperature_mean-25),1);

  // Draw lid temperature
  update_lid_status(sensorValue_lid);
  display.setTextColor(WHITE, BLACK);  // Set text color to white with a black background.
  display.setCursor(128 - 10*6, 56);
  display.print("Lid: ");
  
  if(lid_connected){
    // Display lid temperature if the lid is connected.
    display.print((int)temperature_lid_mean);  // Display the lid temperature as an integer.
    display.print("\xA7");  // Degree symbol.
    display.print("C");
  } else {
    // Display "OFF" if the lid is not connected.
    display.print("OFF");
  }
  
  // Display the updated content on the screen.
  display.display();
}

// rotate is called anytime the rotary inputs change state.
// This funcion is called by interrupts.
void rotate() {
  delayMicroseconds(500) ;

  unsigned char result = rotary.process();
  if (result == DIR_CW) {
    counter++;
    // Serial.println(counter);
  } else if (result == DIR_CCW) {
    counter--;
    // Serial.println(counter);
  }
}

int power_heating(float temperature,float power) {
  // Convert power to an "int" value for the PWM output, used by analogWrite.
  float power_return;

  #define FA_c 0.00564542
  #define FA_d 0.0418254
  #define FA_e 0.000019826
  #define FA_f 0.003711

  power_return = (FA_d-FA_f*temperature-power)/(FA_e*temperature-FA_c);

  // Limit the output from 0 to 255.
  if (power_return>255) power_return=255;
  if (power_return<0) power_return=0;

  return (int)power_return;
}
  
int power_cooling(float temperature,float power){
  float power_return;
    
  #define FA_i 0.000072781
  #define FA_j 0.00413579
  #define FA_k 0.001372876
  #define FA_l 0.1204656

  power_return =(FA_l-FA_j*temperature-power)/(FA_i*temperature-FA_k);

  if (power_return>255) power_return=255;
  if (power_return<0) power_return=0;

  return (int)power_return;
}

void runPID(){
  TEMPcontrol = PIDp*TEMPdif+PIDd*TEMPd+(int)PIDIntegration*PIDi*TEMPi;
  setHeater(temperature_mean, TEMPcontrol, heaterPin, fanPin);
}

void runLidPID(){
  // TODO: choose new PID constants for the lid.
  lidTEMPcontrol = PIDp*lid_TEMPdif + PIDd*lid_TEMPd + (int)lid_PIDIntegration*PIDi*lid_TEMPi;
  setHeater(temperature_lid_mean, lidTEMPcontrol, lidPin, -1);
}

void setHeater(float temperature, float power, int heater_pin, int fan_pin){
  if (power==0) {
    // Power off.
    if(fan_pin > -1) pinMode(fan_pin, OUTPUT);        // fanPin
    if(fan_pin > -1) digitalWrite(fan_pin, LOW);
    if(heater_pin > -1) pinMode(heater_pin, OUTPUT);  // heaterPin
    if(heater_pin > -1) digitalWrite(heater_pin, LOW);
    // Update output.
    heatPower = LOW;
  } else {
    if (power>0){
      // Heater power on.
      heatPower=power_heating(temperature, power); // e.g. 0-255
      if(heater_pin > -1) analogWrite(heater_pin, heatPower);
      // Fan power off.
      if(fan_pin > -1) analogWrite(fan_pin, 0);
    } else {
      // Heater power off.
      if(heater_pin > -1) analogWrite(heater_pin, 0);
      // Fan power on.
      fanPower=power_cooling(temperature, power); // e.g. 0-255
      if(fan_pin > -1) analogWrite(fan_pin, fanPower);
    } 
  }
}
