# Firmware for the PocketPCR

PocketPCR PCR Thermocycler by GaudiLabs, forked by naikymen.

Written for the SAMD21G18B-A microcontroller (used in the Arduino Zero).

Original designs:

- <http://gaudi.ch/PocketPCR/>
- <https://github.com/GaudiLabs/PocketPCR>

Modifications (this version) by Nicolás Méndez:

- <https://gitlab.com/pipettin-bot/forks/labware/PocketPCR>

More information:

- Microcontroller datasheet: <https://ww1.microchip.com/downloads/aemDocuments/documents/MCU32/ProductDocuments/DataSheets/SAM-D21-DA1-Family-Data-Sheet-DS40001882H.pdf>
- Arduino Zero Pinout: <https://content.arduino.cc/assets/Pinout-ZERO_latest.pdf>

# Libraries

Install the required libraries in the Arduino.

- ArduinoJSON: <https://arduinojson.org/>
- Adafruit_GFX
- Adafruit_SSD1306
- The "Rotary" library is already included.

# Installation

1. Open the Arduino IDE.
2. Select the Arduino Zero using the boards manager ("Native USB" option).
3. Open the project at [PocketPCR_V2_quite_dev_i_disp.ino](./PocketPCR_V2_quite_dev_i_disp.ino).
4. Connect the PocketPCR via USB, and upload the firmware.
