# JSON-RPC API Documentation

The `parse_json_from_serial` function is responsible for parsing JSON RPC messages received from the serial interface and executing the corresponding methods based on the provided JSON content. Below are the supported methods and examples of expected input and output.

## Status reports

The `send_alive` function serves the purpose of periodically sending status information about the device via the serial interface. It sends an "alive" message every 5 seconds, providing information about the current device state and its settings in a JSON format. This can be helpful for monitoring and diagnostics, allowing external systems or monitoring tools to keep track of the device's status and configuration.

Here's an example JSON document that the `send_alive` function sends to the serial interface:

```json
{
  "method": "status",
  "data": {
    "state": 1,
    "settings": [94,60,94,120,55,80,72,40,25,180,25,1],
    "timestamp": 123456789,
  }
}
```

Explanation of the JSON document elements:

- `"method"`: This field indicates that the message type is a status update. In this case, it's set to "status."
- `"data"`: This field contains the actual status information, including the device state and settings.
  - `"state"`: This sub-field contains the current state of the device, represented by the `caseUX` variable. It indicates whether the device is in the main menu, settings menu, running a PCR, or other possible states.
  - `"settings"`: This sub-field contains an array of device settings. A for loop iterates through the settings stored in the `settings` array and adds each value to the JSON array.
  - `"timestamp"`: This sub-field contains the current clock time of the device (in milliseconds) since it powered on.

## Supported Methods

### `example`

This method is used for demonstration purposes.

#### Example Input

```json
{
  "method": "example",
  "timestamp": 1234,
  "value": 687,
  "id": 10
}
```

#### Example Response

```json
{
  "id": 10,
  "timestamp": 1234,
  "value": 687
}
```

### `get`

This method retrieves temperature measurement data related to the PCR device.

#### Example Input

```json
{
  "method": "get",
  "id": 42
}
```

#### Example Response

```json
{
  "method": "get",
  "id": 42,
  "data": {
    "lid": [current_lid_temperature],
    "blk": [current_block_temperature],
    "blkset": [block_temperature_setpoint],
    "lidset": [lid_temperature_setpoint]
  }
}
```

### `set_state`

This method sets the state of the PCR device.

Valid states: "run", "stop", "settings", "main".

#### Example Input
```json
{
  "method": "set_state",
  "data": {
    "state": "run"
  }
}
```

#### Example Response

The method will set the PCR device's user interface to the specified state.

Will return an "error" if it fails.

### `configure`

This method configures the cycling settings of the PCR device.

#### Example Input
```json
{
  "method": "configure",
  "data": {
    "settings": [94, 60, 94, 120, 55, 80, 72, 40, 25, 180, 25, 1]
  }
}
```

#### Example Output

The method will update the PCR device's settings based on the provided configuration.

## Usage

1. Connect to the Arduino via the Serial interface.
2. Send a JSON message with the method and corresponding data.
3. The function will parse the message, execute the specified method, and return a response in JSON format.

**Note:** Be sure to replace placeholders like `[current_lid_temperature]`, `[current_block_temperature]`, `[block_temperature_setpoint]`, and `[lid_temperature_setpoint]` with actual values.

For detailed examples and further development, refer to [ArduinoJSON's official documentation](https://arduinojson.org/v6/).
